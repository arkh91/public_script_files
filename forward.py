from scapy.all import *

def get_mac(ip_address):
    """
    Returns the MAC address for the given IP address, or None if it can't be found
    """
    responses, unanswered = srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=ip_address), timeout=2, retry=10, verbose=False)
    for s, r in responses:
        return r[Ether].src
    return None

def forward_packet(packet, new_dst_ip):
    """
    Modifies the packet to change its destination IP address and forwards it
    """
    # Get the MAC address for the new destination IP
    new_dst_mac = get_mac(new_dst_ip)
    
    if new_dst_mac:
        # Modify the packet's destination IP and MAC addresses
        packet[Ether].dst = new_dst_mac
        packet[IP].dst = new_dst_ip

        # Delete checksums and lengths, scapy will recalculate them
        del packet[IP].chksum
        del packet[IP].len
        if TCP in packet:
            del packet[TCP].chksum

        # Send the modified packet
        sendp(packet, verbose=False)
    else:
        print(f"Could not find MAC address for {new_dst_ip}")

def packet_handler(packet):
    """
    Handles each sniffed packet, decides whether and how to forward it
    """
    # Check if this is an IP and TCP packet with a destination port of 22
    if IP in packet and TCP in packet and packet[TCP].dport == 22:
        forward_packet(packet, '51.20.114.48')
    # Otherwise, check if this is an IP packet and forward it to server2
    elif IP in packet:
        forward_packet(packet, '13.48.204.94')

# Enable IP forwarding
with open('/proc/sys/net/ipv4/ip_forward', 'w') as ipf:
    ipf.write('1\n')

# Start sniffing and handling packets
sniff(filter="ip", prn=packet_handler)




#wget https://gitlab.com/arkh91/public_script_files/-/raw/main/forward.py && chmod a+x forward.py && python3 forward.py
