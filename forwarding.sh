#!/bin/bash

sysctl net.ipv4.ip_forward=1
iptables -t nat -A PREROUTING -p tcp --dport 22 -j DNAT --to-destination ipiran
iptables -t nat -A PREROUTING -j DNAT --to-destination khareji
iptables -t nat -A POSTROUTING -j MASQUERADE

#wget https://gitlab.com/arkh91/public_script_files/-/raw/main/forwarding.sh?inline=false && chmod a+x forwarding.sh
